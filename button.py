# coding:utf-8 #when you need to input chinese
# This python document should be put firstly in the old Acer desktop /home/pepper/catkin_ws/src
# Author: Chuang YU  
# Date: 12-04-2018  
# Email: alexchauncy(at)gmail.com

from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
import sys
import subprocess #executing command line
#multi thread or command lines in python


import os ##os.system() execute multi command lines

class window(QtWidgets.QMainWindow):

    def __init__(self, master=None):
        
        QtWidgets.QMainWindow.__init__(self, master)

#add
     
        self.title = 'Facial_feature&ROI_extraction_Chuang_ENSTA'
        self.left = 10
        self.top = 10
        self.width = 600
        self.height = 400
        
# emotion dictionary
        self.emotion_order_match_dicitonary = {"11n":"neutral","23n":"neutral","34n":"neutral","42n":"neutral","51n":"neutral","12h":"happy","21h":"happy","33h":"happy","44h":"happy","52h":"happy","13a":"angry","24a":"angry","32a":"angry","41a":"angry","53a":"angry","14s":"sad","22s":"sad","31s":"sad","43s":"sad","54s":"sad"}

# the header of the .bag name
        self.thermal_data_bag_name="No name"
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        #add
        self.widget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.widget)

################################   1-launch
        self.launch_button = QtWidgets.QPushButton("launch",self.widget)
        self.launch_button.clicked.connect(self.launch_button_function)
        self.launch_button.move(300,160) #move (horizon,vertical)
################################   input Part
#name title        
        self.name_s = QtWidgets.QLabel('Name',self.widget)
        self.name_s.move(150,60)
#emotion order title          
        self.emotion_order = QtWidgets.QLabel('Emotion_Order',self.widget)
        self.emotion_order.move(150,140)  
#walking or standing state title          
        self.walking_standing_state = QtWidgets.QLabel('walking/standing',self.widget)
        self.walking_standing_state.move(300,60) 
        
#name edit
        self.name_edit = QtWidgets.QLineEdit(self)
        self.name_edit.move(150,80)
#emotion order edit   
        self.emotion_order_edit = QtWidgets.QLineEdit(self)
        self.emotion_order_edit.move(150,160) 
#ewalking or standing state edit    
        self.walking_standing_state_edit = QtWidgets.QLineEdit(self)
        self.walking_standing_state_edit.move(300,80) 

########################
        #print (self.name_edit_s.text())
        self.show()

#######################  Launch
    def launch_button_function (self) :   
        print("launch button function!!!")
        #print("3-pepper speak \"Stay start place\" !!!")
        #participant:= artudo regions:=noface start:=15 duration:=5 condition:=standing emotion:=angry order:=13a
        subprocess.Popen("roslaunch facial_temp extract_regions.launch "+"start:=" + " "+" duration:="+" "+"order:=" + emotion_order_edit.text()+" "+"emotion:="+emotion_order_match_dicitonary[emotion_order_edit.text()]+" "+" ",shell=True)
       
        #subprocess.Popen("roslaunch optris_drivers optris_drivers.launch",shell=True)
        #subprocess.Popen("python  ~/catkin_ws/src/thermal_data_chuangyu20180816/src/Show_Optris_Thermal_camera.py",shell=True)
        #subprocess.Popen("rosbag record -O "+self.name_edit_s.text()+"_happy_"+self.times_edit_s.text()+" /optris/thermal_image  /optris/thermal_image_view /optris/image_raw",shell=True)
        #subprocess.Popen("rosbag play "+self.name_edit_p.text()+"_happy_"+self.times_edit_p.text()+".bag",shell=True)
        #subprocess.Popen("python ~/catkin_ws/src/thermal_data_chuangyu20180816/src/Show_Optris_Thermal_camera.py",shell=True)

app = QtWidgets.QApplication(sys.argv)
ex=window()
#ex.show()

app.exec_()
