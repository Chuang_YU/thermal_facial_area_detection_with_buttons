# coding:utf-8 #when you need to input chinese
# This python document should be put firstly in the old Acer desktop /home/pepper/catkin_ws/src
# Author: Chuang YU  
# Date: 12-04-2018  
# Email: alexchauncy(at)gmail.com

from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
import sys
import subprocess #executing command line
#multi thread or command lines in python


import os ##os.system() execute multi command lines

class window(QtWidgets.QMainWindow):

    def __init__(self, master=None):
        
        QtWidgets.QMainWindow.__init__(self, master)

#add
     
        self.title = 'Facial_feature&ROI_extraction_Chuang_ENSTA'
        self.left = 10
        self.top = 10
        self.width = 600
        self.height = 400
#.....................................................................................................................................................................
        ###### 
        # participant
        self.dictionary_participant= [{'walking_duration': '2', 'emotion_order': '11n', 'walking_start': '3', 'walking_end': '5', 'note': 'normal', 'name': 'ziyi', 'standing_end': '31', 'standing_start': '21', 'standing_duration': '10'}, {'walking_duration': '3', 'emotion_order': '12h', 'walking_start': '3', 'walking_end': '6', 'note': '', 'name': 'ziyi', 'standing_end': '30', 'standing_start': '20', 'standing_duration': '10'}, {'walking_duration': '5', 'emotion_order': '13a', 'walking_start': '0', 'walking_end': '5', 'note': '', 'name': 'ziyi', 'standing_end': '27', 'standing_start': '18', 'standing_duration': '9'}, {'walking_duration': '4', 'emotion_order': '14s', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '26', 'standing_start': '16', 'standing_duration': '10'}, {'walking_duration': '4', 'emotion_order': '21h', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '25', 'standing_start': '15', 'standing_duration': '10'}, {'walking_duration': '4', 'emotion_order': '22s', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '25', 'standing_start': '15', 'standing_duration': '10'}, {'walking_duration': '4', 'emotion_order': '23n', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '27', 'standing_start': '17', 'standing_duration': '10'}, {'walking_duration': '4', 'emotion_order': '24a', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '27', 'standing_start': '17', 'standing_duration': '10'}, {'walking_duration': '4', 'emotion_order': '31s', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '26', 'standing_start': '16', 'standing_duration': '10'}, {'walking_duration': '4', 'emotion_order': '32a', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '25', 'standing_start': '16', 'standing_duration': '9'}, {'walking_duration': '3', 'emotion_order': '33h', 'walking_start': '0', 'walking_end': '3', 'note': '', 'name': 'ziyi', 'standing_end': '25', 'standing_start': '15', 'standing_duration': '10'}, {'walking_duration': '4', 'emotion_order': '34n', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '24', 'standing_start': '14', 'standing_duration': '10'}, {'walking_duration': '2', 'emotion_order': '41a', 'walking_start': '0', 'walking_end': '2', 'note': '', 'name': 'ziyi', 'standing_end': '24', 'standing_start': '14', 'standing_duration': '10'}, {'walking_duration': '4', 'emotion_order': '42n', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '23', 'standing_start': '13', 'standing_duration': '10'}, {'walking_duration': '5', 'emotion_order': '43s', 'walking_start': '0', 'walking_end': '5', 'note': '', 'name': 'ziyi', 'standing_end': '28', 'standing_start': '18', 'standing_duration': '10'}, {'walking_duration': '3', 'emotion_order': '44h', 'walking_start': '0', 'walking_end': '3', 'note': '', 'name': 'ziyi', 'standing_end': '22', 'standing_start': '12', 'standing_duration': '10'}, {'walking_duration': '4', 'emotion_order': '51n', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '26', 'standing_start': '16', 'standing_duration': '10'}, {'walking_duration': '4', 'emotion_order': '52h', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '24', 'standing_start': '15', 'standing_duration': '9'}, {'walking_duration': '4', 'emotion_order': '53a', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '26', 'standing_start': '17', 'standing_duration': '9'}, {'walking_duration': '4', 'emotion_order': '54s', 'walking_start': '0', 'walking_end': '4', 'note': '', 'name': 'ziyi', 'standing_end': '24', 'standing_start': '15', 'standing_duration': '9'}]


#.....................................................................................................................................................................
        
# emotion dictionary
        self.emotion_order_match_dicitonary = {"11n":"neutral","23n":"neutral","34n":"neutral","42n":"neutral","51n":"neutral","12h":"happy","21h":"happy","33h":"happy","44h":"happy","52h":"happy","13a":"angry","24a":"angry","32a":"angry","41a":"angry","53a":"angry","14s":"sad","22s":"sad","31s":"sad","43s":"sad","54s":"sad"}

# the header of the .bag name
        self.thermal_data_bag_name="No name"
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        #add
        self.widget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.widget)

################################   1-launch
        self.launch_button = QtWidgets.QPushButton("launch",self.widget)
        self.launch_button.clicked.connect(self.launch_button_function)
        self.launch_button.move(300,160) #move (horizon,vertical)
################################   2-input Part
#name title        
        self.name_s = QtWidgets.QLabel('Name',self.widget)
        self.name_s.move(150,60)
#emotion order title          
        self.emotion_order = QtWidgets.QLabel('Emotion_Order',self.widget)
        self.emotion_order.move(150,140)  
#walking or standing state title          
        self.walking_standing_state = QtWidgets.QLabel('walking/standing',self.widget)
        self.walking_standing_state.move(300,60) 
        
#name edit
        self.name_edit = QtWidgets.QLineEdit(self)
        self.name_edit.move(150,80)
#emotion order edit   
        self.emotion_order_edit = QtWidgets.QLineEdit(self)
        self.emotion_order_edit.move(150,160) 
#ewalking or standing state edit    
        self.walking_standing_state_edit = QtWidgets.QLineEdit(self)
        self.walking_standing_state_edit.move(300,80) 
################################   3-clear
        self.clear_button = QtWidgets.QPushButton("clear",self.widget)
        self.clear_button.clicked.connect(self.clear_button_function)
        self.clear_button.move(460,160) #move (horizontal,vertical)

########################
        #print (self.name_edit_s.text())
        self.show()



#######################  1-Launch
    def launch_button_function (self) :   
        print("launch_button_function_by Chuang")
        
        #print("3-pepper speak \"Stay start place\" !!!")
        #participant:= artudo regions:=noface start:=15 duration:=5 condition:=standing emotion:=angry order:=13a
#.....................................................................................................................................................................      
        for dictionary_chuang in self.dictionary_participant:
            if dictionary_chuang["emotion_order"]== self.emotion_order_edit.text():
                start_time_str = dictionary_chuang['walking_start']
                duration_str   = dictionary_chuang['walking_duration']
        subprocess.Popen("cd ~/catkin_ws",shell=True)
        subprocess.Popen("roslaunch facial_temp extract_regions.launch"+" "+"start:=" + start_time_str+" "+"duration:="+duration_str+" "+"order:=" + self.emotion_order_edit.text()+" "+"emotion:="+self.emotion_order_match_dicitonary[self.emotion_order_edit.text()]+" ",shell=True)
#.....................................................................................................................................................................
#######################  1-clear
    def clear_button_function (self) :   
        subprocess.Popen("clear",shell=True)   
        print("clear_button_function_by Chuang")   
       
        #subprocess.Popen("roslaunch optris_drivers optris_drivers.launch",shell=True)
        #subprocess.Popen("python  ~/catkin_ws/src/thermal_data_chuangyu20180816/src/Show_Optris_Thermal_camera.py",shell=True)
        #subprocess.Popen("rosbag record -O "+self.name_edit_s.text()+"_happy_"+self.times_edit_s.text()+" /optris/thermal_image  /optris/thermal_image_view /optris/image_raw",shell=True)
        #subprocess.Popen("rosbag play "+self.name_edit_p.text()+"_happy_"+self.times_edit_p.text()+".bag",shell=True)
        #subprocess.Popen("python ~/catkin_ws/src/thermal_data_chuangyu20180816/src/Show_Optris_Thermal_camera.py",shell=True)

app = QtWidgets.QApplication(sys.argv)
ex=window()
#ex.show()

app.exec_()
